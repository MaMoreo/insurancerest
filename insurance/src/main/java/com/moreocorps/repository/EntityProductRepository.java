package com.moreocorps.repository;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;

import com.moreocorps.model.EntityProduct;

public interface EntityProductRepository extends JpaRepository<EntityProduct, Long> {
	
	/**
	 * Finder method that dereferences the username property 
	 * on the Bookmark entity�s Account relationship, 
	 * ultimately requiring a join of some sort. 
	 * 
	 * The JPA query it generates is, roughly: 
	 * SELECT b from Bookmark b WHERE b.account.username = :username.
	 * 
	 * @param username username in Account to be found
	 * @return A collection with all the accouts for this user.
	 */
	Collection<EntityProduct> findByUserName(String username);

}
