package com.moreocorps.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.moreocorps.model.User;

public interface UserRepository extends JpaRepository<User, Long> {

	/**
	 * Custom finder-method. It creates a JPA query of the form <br>
	 * "select a from User a where a.username = :username," run it (passing in
	 * the method <br>
	 * argument username as a named parameter for the query), and return the
	 * results for us.
	 * 
	 * @param username
	 *            the username to match
	 * @return The account if found.
	 */
	Optional<User> findByName(String username);
}
