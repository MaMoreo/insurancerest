package com.moreocorps.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;


@SuppressWarnings("serial")
@ResponseStatus(HttpStatus.NOT_FOUND)
public class ProductNotFoundException extends RuntimeException {

	/**
	 * The bookmark Id was not found for the specified user.
	 */
	public ProductNotFoundException(String userId, Long productId) {
		super("could not find product '" + productId + "' for '" + userId + "'.");
	}

	/**
	 * The bookmark does not exist in this system.
	 * 
	 * @param bookmarkId Id was not found in the system.
	 */
	public ProductNotFoundException(Long productId) {
		super("could not find product '" + productId + "'.");
	}
}
