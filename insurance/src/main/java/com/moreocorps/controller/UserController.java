package com.moreocorps.controller;

import java.net.URI;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.moreocorps.model.Bike;
import com.moreocorps.model.EntityProduct;
import com.moreocorps.model.InsuranceProduct;
import com.moreocorps.model.Jewelry;
import com.moreocorps.model.User;
import com.moreocorps.repository.EntityProductRepository;
import com.moreocorps.repository.UserRepository;

@RestController
@RequestMapping("/{userId}/products")
public class UserController {

	private final UserRepository userRepository;
	private final EntityProductRepository productRepository;

	@Autowired
	public UserController(UserRepository userRepository, EntityProductRepository productRepository) {
		this.userRepository = userRepository;
		this.productRepository = productRepository;
	}

	/**
	 * Gets all the products for the specified user . Replies to
	 * /{userId}/products (GET)
	 * 
	 * @return A collection with all the bookmarks
	 */
	@RequestMapping(method = RequestMethod.GET)
	Collection<EntityProduct> getAllBookmarksForUser(@PathVariable String userId) {
		this.validateUser(userId);
		return productRepository.findByUserName(userId);
	}

	/**
	 * Cheks if the user exists. If not, throws an Exception
	 * 
	 * @param userId
	 *            the userId, (normaly the name) to be checked.
	 */
	private void validateUser(String userId) {
		userRepository.findByName(userId).orElseThrow(() -> new UserNotFoundException(userId));
	}

	/**
	 * Gets all the products for the specified user . Replies to
	 * /{userId}/products/{productId} (GET)
	 * 
	 * @return A collection with all the B
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/{productId}")
	EntityProduct getAllBookmarksForUser(@PathVariable String userId, @PathVariable Long productId) {
		this.validateUser(userId);
		EntityProduct productToFind = validateProduct(productId);

		if (findProductInAccount(userId, productToFind)) {
			return productToFind;
		} else {
			throw new ProductNotFoundException(userId, productId);
		}
	}

	/**
	 * Finds a bookmark Id in the specified user account.
	 * 
	 * @param userName
	 * @param productToFind
	 * @return
	 */
	private Boolean findProductInAccount(String userName, EntityProduct productToFind) {
		Collection<EntityProduct> products = productRepository.findByUserName(userName);
		Boolean found = false;
		for (EntityProduct product : products) {
			if (product.getId() == productToFind.getId()) {
				found = true;
			}
		}
		return found;
	}
	
	/**
	 * Cheks if the bookmark exists in the system. If not, throws an Exception
	 * 
	 * @param bookmarkId
	 *            the id to be checked.
	 * @return the Bookmak if found, throws a {@link BookmarkNotFoundException}
	 *         otherwise
	 */
	private EntityProduct validateProduct(Long productId) {
		EntityProduct findOne = this.productRepository.findOne(productId);
		if (findOne == null) {
			throw new ProductNotFoundException(productId);
		}
		return findOne;
	}
	
	/**
	 * Creates a product for an existing user. Replies to /{userId}/products
	 * (POST) <br>
	 * <br>
	 * 
	 * Example of curl call: <br>
	 * curl -H "Content-Type: application/json" -X POST -d
	 * {\"uri\":\"http://bookmark.com/1/miguel\"}
	 * http://localhost:8080/miguel/bookmarks <br>
	 * <br>
	 * curl -H "Content-Type: application/json" -X POST -d
	 * "{\"input\":0}"
	 * http://localhost:8080/miguel/bookmarks <br>
	 * 
	 * @param userId
	 * @param input
	 * @return the created entity if any
	 */
	@RequestMapping(method = RequestMethod.POST)
	ResponseEntity<?> create(@PathVariable String userId, @RequestBody EntityProduct input) {
		this.validateUser(userId);

		return this.userRepository.findByName(userId).map(account -> {
			InsuranceProduct p;
			if (input.getType() == 0){
				p = new Bike();
			}else{
				p = new Jewelry();
			}
			
			User s = this.userRepository.findByName(userId).get();
			EntityProduct result = productRepository.save(new EntityProduct(p, s));
			URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(result.getId())
					.toUri();

			return ResponseEntity.created(location).build();
		}).orElse(ResponseEntity.noContent().build());
	}
}
