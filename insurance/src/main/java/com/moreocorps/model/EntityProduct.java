package com.moreocorps.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Database table
 * 
 * @author Mike
 *
 */
@Entity
public class EntityProduct {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@NotNull
	private Integer coverageLow;
	@NotNull
	private Integer coverageHigh;
	@NotNull
	private Integer risk;
	
	@JsonIgnore
	@ManyToOne
	private User user;
	
	
	//private ProductType   // Como meto un ENUM en la base de datos???
	private Integer Type = 0; // 0 = Bike, 1 = Jewelry etc etc
	
	public EntityProduct(){
		// JPA 
	}
	
	public EntityProduct(InsuranceProduct product, User user) {
		this.coverageHigh = product.getCoverageHigh();
		this.coverageLow = product.getCoverageLow();
		this.risk = product.getRisk();
		if (product instanceof Bike){  //Real bad idea
			this.Type = 0;
		}else {
			this.Type = 1;
		}
		this.user = user;
	}

	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setCoverageLow(Integer coverageLow) {
		this.coverageLow = coverageLow;
	}

	public void setCoverageHigh(Integer coverageHigh) {
		this.coverageHigh = coverageHigh;
	}

	public void setRisk(Integer risk) {
		this.risk = risk;
	}

	public int getCoverageLow(){
		return coverageLow;
	}
	
	public int getCoverageHigh(){
		return coverageHigh;
	}

	public int getRisk() {
		return risk;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Integer getType() {
		return Type;
	}

	public void setType(Integer type) {
		Type = type;
	}
	
	
}
