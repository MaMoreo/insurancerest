package com.moreocorps.model;

public class Bike extends InsuranceProduct {

	protected final Integer coverageLow = 0;
	protected final Integer coverageHigh = 3000;
	protected final Integer risk = 30;
	
	/**
	 * Fields: ID: Unique Id to identify this resource.
	 */
	
	@Override
	public int getCoverageLow(){
		return coverageLow;
	}
	
	@Override
	public int getCoverageHigh(){
		return coverageHigh;
	}

	@Override
	public int getRisk() {
		return risk;
	}
}
