package com.moreocorps.model;

/**
 * Abstract Class that contains the basic functionality 
 * of all the products that can be taken under insurance. 
 */
public abstract class InsuranceProduct {

	/**
	 * Calculates the price to pay for this coverage based on the risk.
	 * 
	 * @param userCoverage The coverage amount for this user.
	 * @return the Price based on the risk.
	 */
	public int calculatePrice(int userCoverage) {
		return (userCoverage * getRisk()) / 100;
	}

	/**
	 * Checks if the user its in the range.
	 * 
	 * @param userCoverage The coverage amount for this user.
	 * @return {@code true} if the coverage is in the allowed range,
	 * <br> {@code false} otherwise. 
	 */
	public boolean checkCoverage(Integer userCoverage) {
		if (userCoverage >= getCoverageLow() && userCoverage <= getCoverageHigh()) {
			return true;
		}
		return false;
	}
	
	// 	Getters ... Setters...
	public abstract int getCoverageLow();
	public abstract int getCoverageHigh();
	public abstract int getRisk();
}
