package com.moreocorps;

import java.util.Arrays;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.moreocorps.model.Bike;
import com.moreocorps.model.EntityProduct;
import com.moreocorps.model.Jewelry;
import com.moreocorps.model.User;
import com.moreocorps.repository.EntityProductRepository;
import com.moreocorps.repository.UserRepository;

@SpringBootApplication
public class InsuranceApplication {

	public static void main(String[] args) {
		SpringApplication.run(InsuranceApplication.class, args);
	}
	
	
	
	@Bean
	CommandLineRunner init(UserRepository userRepository, EntityProductRepository productRepository) {
		// CommandLineRunner runner = (String [] evt) -> body of the run method
		// parameter of the run method -> body of the run method
		return (evt) -> Arrays.asList("filemon,mortadelo".split(",")).forEach(a -> {
			User user = userRepository.save(new User(a, 0));
			productRepository.save(new EntityProduct(new Bike(), user));  // p1
			productRepository.save(new EntityProduct(new Jewelry(), user));  // p2
		});
	}
}
